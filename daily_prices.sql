create table daily_prices
(
    ticker text not null,
    date   date not null,
    price  numeric,
    constraint daily_prices_pk
        primary key (date, ticker)
);

