import logging

from sqlalchemy import create_engine, text

from src.utils.config import postgres_url


def sql_statement_to_dict(statement: str):
    try:
        engine = create_engine(postgres_url)
        engine.connect()
        sql = text(statement)
        return [dict(row) for row in engine.execute(sql)]
    except Exception as e:
        logging.getLogger().exception(e)
    finally:
        engine.dispose()
